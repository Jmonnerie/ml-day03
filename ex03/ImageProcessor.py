# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    ImageProcessor.py                                  :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/29 12:18:24 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/29 22:45:21 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#


import os
import time
import getpass
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


logg = open("./machine.log", "w+")


def log(func):
    def wrap(*args, **kwargs):
        username = getpass.getuser()

        time1 = time.time()
        ret = func(*args, *kwargs)
        time2 = time.time()

        timer = (time2-time1) * 1000
        measure = "ms"
        if timer >= 1000:
            measure = "s"
            timer = timer / 1000
        logg.write('({})Running: {:13} [ exec-time = {:5.3f} {:2} ]\n'
                   .format(username, func.__name__, timer, measure))
        logg.write("\tin  > " + str(args) + " " + str(kwargs) + "\n")
        logg.write("\tout > " + str(ret) + "\n")
        return ret
    return wrap


class ImageProcessor:
    @staticmethod
    @log
    def load(filename):
        tab = np.array(mpimg.imread(filename))
        print(f"Loading image of dimensions {tab.shape[0]} x {tab.shape[1]}")
        return tab

    @staticmethod
    @log
    def display(arr):
        plt.imshow(arr)
        plt.show()


if __name__ == "__main__":
    ipr = ImageProcessor()
    imgTab = ipr.load("https://github.com/42-AI/bootcamp_python/blob/master/" +
                      "day03/resources/42AI.png?raw=true")
    print("load return type : ", type(imgTab))
    ipr.display(imgTab)
    input("Press enter to display a cute thing")
    imgTab = ipr.load('https://matplotlib.org/3.2.1/_images/stinkbug.png')
    print("load return type : ", type(imgTab))
    ipr.display(imgTab)
    logg.close()
