# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    ColorFilter.py                                     :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/29 19:42:49 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/29 19:42:50 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

import os
import time
import getpass
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from ImageProcessor import ImageProcessor


logg = open("./machine.log",  "w+")


def log(func):
    def wrap(*args,  **kwargs):
        username = getpass.getuser()

        time1 = time.time()
        ret = func(*args,  *kwargs)
        time2 = time.time()

        timer = (time2-time1) * 1000
        measure = "ms"
        if timer >= 1000:
            measure = "s"
            timer = timer / 1000
        logg.write('({})Running: {:13} [ exec-time = {:5.3f} {:2} ]\n'
                   .format(username,  func.__name__,  timer,  measure))
        logg.write("\tin  > " + str(args) + " " + str(kwargs) + "\n")
        logg.write("\tout > " + str(ret) + "\n")
        return ret
    return wrap


class ColorFilter:
    @staticmethod
    @log
    def invert(array):
        return 1 - array

    @staticmethod
    @log
    def to_blue(array):
        tab = np.zeros(array.shape)
        for row in range(array.shape[0]):
            for col in range(array.shape[1]):
                tab[row][col][2] = array[row][col][2]
        return tab

    @staticmethod
    @log
    def to_green(array):
        return [0, 1, 0] * array

    @staticmethod
    @log
    def to_red(array):
        return array - ColorFilter.to_blue(array) - ColorFilter.to_green(array)

    @staticmethod
    @log
    def celluloid(array, n=4):
        lin = np.linspace(0, 1, num=n)
        cop = 1 * array
        for r, row in enumerate(array):
            for p, pixel in enumerate(row):
                for c, color in enumerate(pixel):
                    for thresold in lin:
                        if -1 / (n * 2) <= (thresold - color) <= 1 / (n * 2):
                            cop[r][p][c] = thresold
        return cop


if __name__ == "__main__":
    cf = ColorFilter()
    ip = ImageProcessor()
    img = ip.load("https://github.com/42-AI/bootcamp_python/blob/master/" +
                  "day03/resources/42AI.png?raw=true")
    to_print = [img]
    print("========================== invert img")
    to_print += [cf.invert(img)]
    print("========================== to_blue img")
    to_print += [cf.to_blue(img)]
    print("========================== to_green img")
    to_print += [cf.to_green(img)]
    print("========================== to_red img")
    to_print += [cf.to_red(img)]
    print("========================== celluloid img")
    to_print += [cf.celluloid(img)]
    print("========================== print img")
    glob = img
    for elem in to_print:
        ip.display(elem)
        glob = np.concatenate((glob, elem), 0)
    ip.display(glob)
