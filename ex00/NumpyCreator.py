# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    NumpyCreator.py                                    :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/29 09:48:27 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/29 23:20:22 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

import os
import time
import getpass
import unittest
import numpy as np


logg = open("./machine.log", "w+")


def log(func):
    def wrap(*args, **kwargs):
        username = getpass.getuser()

        time1 = time.time()
        ret = func(*args, *kwargs)
        time2 = time.time()

        timer = (time2-time1) * 1000
        measure = "ms"
        if timer >= 1000:
            measure = "s"
            timer = timer / 1000
        logg.write('({})Running: {:13} [ exec-time = {:5.3f} {:2} ]\n'
                   .format(username, func.__name__, timer, measure))
        logg.write("\tin  > " + str(args) + " " + str(kwargs) + "\n")
        logg.write("\tout > " + str(ret) + "\n")
        return ret
    return wrap


class NumpyCreator:
    @staticmethod
    @log
    def from_list(lst, dtype=None):
        return np.array(lst, dtype)

    @staticmethod
    @log
    def from_tuple(tup, dtype=None):
        return np.array(tup, dtype)

    @staticmethod
    @log
    def from_iterable(it, dtype=None):
        return np.array(it, dtype)

    @staticmethod
    @log
    def from_shape(shape, value=0.):
        return np.full(shape, value)

    @staticmethod
    @log
    def random(shape):
        return np.random.rand(shape)

    @staticmethod
    @log
    def identity(size, dtype=None):
        return np.identity(size, dtype)


class TestNpCreator(unittest.TestCase):

    def assertNp2DArrayEqual(self, expected, output):
        self.assertEqual(type(expected), type(output))
        self.assertEqual(expected.shape, output.shape)
        self.assertTrue(all(
            isinstance(expected[row], type(output[row])) and
                all(
                    isinstance(expected[row], type(output[row])) and
                    expected[row][col] == output[row][col]
                    for col in range(output.shape[1])
                )
            for row in range(output.shape[0])))

    def assertNp1DArrayEqual(self, expected, output):
            self.assertEqual(type(expected), type(output))
            self.assertEqual(expected.shape, output.shape)
            self.assertTrue(all(
                isinstance(expected[col], type(output[col])) and
                expected[col] == output[col]
                for col in range(output.shape[0])
            ))

    def setUp(self):
        self.npc=NumpyCreator()
        self.list=[[1, 2, 3], [6, 3, 4]]
        self.tuple=("a", "b", "c")
        self.range=range(5)
        self.shape=(3, 5)
        self.id=4

    def test_from_list(self):
        expected=np.array([[1, 2, 3], [6, 3, 4]])
        output=self.npc.from_list(self.list)
        self.assertNp2DArrayEqual(expected, output)

    def test_from_tuple(self):
        expected=np.array(['a', 'b', 'c'])
        output=self.npc.from_tuple(self.tuple)
        self.assertNp1DArrayEqual(expected, output)

    def test_from_iterable(self):
        expected=np.array([0, 1, 2, 3, 4])
        output=self.npc.from_iterable(self.range)
        self.assertNp1DArrayEqual(expected, output)

    def test_from_shape(self):
        expected=np.array([[0, 0, 0, 0, 0],
                            [0, 0, 0, 0, 0],
                            [0, 0, 0, 0, 0]])
        output=self.npc.from_shape(self.shape)
        self.assertNp2DArrayEqual(expected, output)

    def test_from_random(self):
        pass

    def test_identity(self):
        expected=np.array([[1., 0., 0., 0.],
                            [0., 1., 0., 0.],
                            [0., 0., 1., 0.],
                            [0., 0., 0., 1.]])
        output=self.npc.identity(self.id)
        self.assertNp2DArrayEqual(expected, output)

    def test_dtype(self):
        print(type(np.array))


if __name__ == "__main__":
    unittest.main()
    logg.close()
