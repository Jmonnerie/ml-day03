# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    ScrapBooker.py                                     :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/29 14:09:22 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/29 19:22:33 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

import os
import time
import getpass
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


logg = open("./machine.log",  "w+")


def log(func):
    def wrap(*args,  **kwargs):
        username = getpass.getuser()

        time1 = time.time()
        ret = func(*args,  *kwargs)
        time2 = time.time()

        timer = (time2-time1) * 1000
        measure = "ms"
        if timer >= 1000:
            measure = "s"
            timer = timer / 1000
        logg.write('({})Running: {:13} [ exec-time = {:5.3f} {:2} ]\n'
                   .format(username,  func.__name__,  timer,  measure))
        logg.write("\tin  > " + str(args) + " " + str(kwargs) + "\n")
        logg.write("\tout > " + str(ret) + "\n")
        return ret
    return wrap


class ScrapBooker:
    def __init__(self):
        pass

    @staticmethod
    @log
    def crop(array,  dimensions,  position=(0,  0)):
        """
            crops the image as a rectangle with the given dimensions
            (meaning,  the new height and width for the image),  whose top left
            corner is given by the position argument. The position should be
            (0, 0) by default. You have to consider it an error (and handle
            said error) if dimensions is larger than the current image size.

            :param array: np.array 2D,  representing an image
            :param dimensions:
            :param position:
            :return:
        """
        if type(dimensions) != tuple or len(dimensions) != 2:
            raise ValueError("dimensions is not a 2 length int tupple")
        dimPos = (dimensions[0] + position[0])
        if dimensions + position > (len(array),  len(array[0])):
            raise ValueError("the array is too much small compared to " +
                             "dimensions + position")
        # https://www.williameunice.com/2019/07/03/slicing-2d-arrays-in-python/
        return array[
            position[1]:dimensions[1]+position[1],
            position[0]:dimensions[0]+position[0]
        ]

    @staticmethod
    @log
    def thin(array,  n,  axis=0):
        """
            deletes every n-th pixel row along the specified axis (0 vertical,
            1 horizontal),  example below.

            :param array:
            :param n:
            :param axis:
        """
        axis = 0 if axis == 1 else 1
        mask = [
            i
            for i in range(array.shape[axis])
            if (i + 1) % n == 0
        ]
        return np.delete(array,  mask,  axis)

    @staticmethod
    @log
    def juxtapose(array,  n,  axis):
        """
            juxtaposes n copies of the image along the specified axis (0
            vertical,  1 horizontal).

            :param array:
            :param n:
            :param axis:
        """
        tab = array.copy()
        for i in range(n - 1):
            tab = np.concatenate((tab,  array),  axis)
        return tab

    @staticmethod
    @log
    def mosaic(array,  dimensions):
        """
            makes a grid with multiple copies of the array. The dimensions
            argument specifies the dimensions (meaning the height and width)
            of the grid (e.g. 2x3).

            :param array:
            :param dimensions:
        """
        return np.tile(array, dimensions)


if __name__ == "__main__":
    t1 = np.array([
        ["A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A"],
        ["B", "B", "B", "B", "B", "B", "B", "B", "B", "B", "B", "B"],
        ["C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C", "C"],
        ["D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D", "D"],
        ["E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E"],
        ["F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F", "F"],
        ["G", "G", "G", "G", "G", "G", "G", "G", "G", "G", "G", "G"],
        ["H", "H", "H", "H", "H", "H", "H", "H", "H", "H", "H", "H"],
        ["I", "I", "I", "I", "I", "I", "I", "I", "I", "I", "I", "I"],
        ["J", "J", "J", "J", "J", "J", "J", "J", "J", "J", "J", "J"],
        ["K", "K", "K", "K", "K", "K", "K", "K", "K", "K", "K", "K"]
    ])
    sb = ScrapBooker()
    print("========================== thin t, 4, 0")
    print(sb.thin(t1,  4,  0))
    print("========================== thin t, 4, 1")
    print(sb.thin(t1,  4,  1))
    print("========================== juxtapose")
    print(sb.juxtapose([t1[0]],  3,  0))
    print("========================== mosaic")
    print(sb.mosaic([t1[0], t1[1]], (3, 2)))
